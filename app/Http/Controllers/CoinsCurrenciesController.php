<?php

namespace App\Http\Controllers;

use App\Repositories\CoinsCurrenciesRepository;
use Illuminate\Http\Request;

class CoinsCurrenciesController extends Controller
{

    protected $coinsCurrenciesRepository;

    public function __construct(CoinsCurrenciesRepository $coinsCurrenciesRepository)
    {
        $this->coinsCurrenciesRepository = $coinsCurrenciesRepository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function coinsCurrencies()
    {
        return $this->coinsCurrenciesRepository->coinsCurrencies();
    }

    /**
     *  All Cons
     *
     * @param Request $request
     * @return void
     */
    public function convert(Request $request)
    {
        return $this->coinsCurrenciesRepository->convert($request);
    }
}
