<?php

namespace App\Repositories;

use App\Contracts\CoinsCurrenciesInterface;
use App\Models\Coin;
use App\Models\Currency;
use Illuminate\Http\Request;

class CoinsCurrenciesRepository implements CoinsCurrenciesInterface
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function convert(Request $request)
    {
        $url = $request->dataUrl;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $dom = new \DOMDocument();
        $dom->loadHTML($response);
        $data = $dom->getElementsByTagName('strong');

        if (isset($data[0]->textContent)) {
            return response()->json([
                'currency' => $data[0]->textContent
            ]);
        }

        return response()->json([
            'currency' => 0
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function coinsCurrencies()
    {
        $coins = Coin::all();
        $currencies = Currency::all();

        return response()->json([
            'coins' => $coins,
            'currencies' => $currencies
        ]);
    }
}