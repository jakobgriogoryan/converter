<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Currency;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class CurrenciesService
{
    const URL = 'https://api.exchangeratesapi.io/';

    /**
     * Get a coin item data
     *
     * @param $symbol
     * @param $price
     * @return array
     */
    public function saveCurrency($symbol, $price)
    {
        $currency = Currency::UpdateOrCreate(
            ['symbol' => $symbol],
            [
                'name' => $symbol,
                'slug' => $symbol,
                'symbol' => $symbol,
                'price' => (float)$price,
                'published_at' => $currency->published_at ?? Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        return $currency->save();
    }

    public function fetchCurrencies()
    {
        $client = new Client([
            'headers' => [
                'Accepts' => 'application/json',
            ],
        ]);

        $limit = 2000;
        try {
            $endpoint = self::URL . 'latest';
            $data = [];
            $skip = 0;
            do {
                $request = $client->get($endpoint, [
                    'query' => [
                        'skip' => $skip,
                        'limit' => $limit,
                    ],
                ]);
                $result = json_decode($request->getBody()->getContents());

                $currencies = $result->rates;
                foreach ($currencies as $key => $currency) {
                    $data[$key] = $currency;
                }
                $skip += $limit;
            } while (count($data) === $limit);

            $rows = [];
            foreach ($data as $symbol => $item) {
                try {
                    $rows[] = $this->saveCurrency($symbol, $item);
                } catch (\Exception $e) {
                    Log::error(['Error' => [
                        'status' => false,
                        'command' => 'Fetch Currencies (' . $symbol . ')',
                        'message' => $e->getMessage(),
                        'code' => $e->getCode(),
                        'line' => $e->getLine(),
                        'file' => $e->getFile(),
                    ]]);
                }
            }

        } catch (\Exception $e) {
            Log::error(['Error' => [
                'status' => false,
                'command' => 'Fetch Currencies',
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
            ]]);
        }

        return ['success' => [
            'status' => true,
        ]];
    }
}
