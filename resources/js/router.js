import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Converter from "./components/Converter";

const routes = [
    {
        path: '/',
        component: Converter
    },
    {
        path: '/convert',
        component: Converter
    }
]

export default new VueRouter({
    mode: "history",
    routes
})
