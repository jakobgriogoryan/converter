import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        coins: [],
        currencies: [],
        currency: ''
    },
    mutations: {
        FETCH_COINS(state, coins) {
            return state.coins = coins
        },
        FETCH_CURRENCIES(state, currencies) {
            return state.currencies = currencies
        },
        FETCH_CONVERSION(state, currency) {
            state.currency = currency
        }
    },
    getters: {
        coins: state => {
            return state.coins
        },
        currencies: state => {
            return state.currencies
        },
        currency: state => state.currency,
    },
    actions: {
        async fetchConversion({commit}, data) {

            let dataUrl = {
                'dataUrl': data.dataUrl
            };

            await axios.post(data.url, dataUrl)
                .then(res => {
                    commit('FETCH_CONVERSION', res.data.currency)
                }).catch(err => {
                    console.log(err)
                })
        },
        async fetchCoinsCurrencies({commit}, url) {
            await axios.get(url)
                .then(res => {
                    commit('FETCH_COINS', res.data.coins);
                    commit('FETCH_CURRENCIES', res.data.currencies);
                }).catch(err => {
                    console.log(err)
                })
        }
    }
})